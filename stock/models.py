from django.db import models

class Product(models.Model):
	name = models.CharField(max_length=100)
	description = models.TextField()
	code = models.TextField(null=True, max_length=12)
	image = models.ImageField(null=True, upload_to = 'products')
	quantity = models.PositiveIntegerField(null=True)

	def __str__(self):
		return self.name
	
